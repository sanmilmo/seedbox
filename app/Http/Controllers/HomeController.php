<?php

namespace ServerCo\Http\Controllers;

use Illuminate\Http\Request;
use ServerCo\Server;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $cloudServers = Server::all();
        return view('home', ['cloudServers' => $cloudServers]);
    }
}
