<?php

namespace ServerCo\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use ServerCo\Server;
use Illuminate\Http\Request;


class ServerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cloudServers = Server::all();
        return view('servers.index')->with('cloudServers', $cloudServers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('servers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        request()->validate([
            'description' => ['required', 'min:3', 'max:255'],
            'name' => ['required', 'min:3', 'max:255'],
            'coresSpeed' => ['required', 'min:3', 'max:15', 'numeric'],
            'bandwidth' => ['required', 'numeric','min:3', 'max:15', 'numeric'],
            'model' => ['required', 'numeric'],
            'ops' => ['required'],
            'drives' => ['required', 'numeric'],
            'driveType' => ['required'],
            'cores' => ['required', 'numeric'],
        ]);

        $server = new Server();
        $server->name = $request->name;
        $server->user_id = Auth::id();
        $server->description = $request->description;
        $server->model = $request->model;
        $server->ops = $request->ops;
        $server->ip = "".mt_rand(0,255).".".mt_rand(0,255).".".mt_rand(0,255).".".mt_rand(0,255);
        $server->drives = $request->drives;
        $server->drive_type = $request->driveType;
        $server->cores = $request->cores;
        $server->cores_speed = $request->coresSpeed;
        $server->bandwidth = $request->bandwidth;
        $server->ddos_protection = $request->ddosProtection ?? false;
        $server->save();
        return redirect('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param Server $server
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Server $server)
    {
        return view('servers.show', ['server' => $server]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \ServerCo\Server  $server
     * @return \Illuminate\Http\Response
     */
    public function edit(Server $server)
    {
        return view('servers.edit', ['server' => $server]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Server $server
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Server $server)
    {
        request()->validate([
            'description' => ['required', 'min:3', 'max:255'],
            'name' => ['required', 'min:3', 'max:255'],
            'coresSpeed' => ['required', 'min:3', 'max:15', 'numeric'],
            'bandwidth' => ['required', 'numeric','min:3', 'max:15', 'numeric'],
            'model' => ['required', 'numeric'],
            'ops' => ['required'],
            'drives' => ['required', 'numeric'],
            'driveType' => ['required'],
            'cores' => ['required', 'numeric'],
        ]);

        $server->name = request('name');
        $server->description = request('description');
        $server->model = request('model');
        $server->ops = request('ops');
        $server->drives = request('drives');
        $server->drive_type = request('driveType');
        $server->cores = request('cores');
        $server->cores_speed = request('cores_speed');
        $server->bandwidth = request('bandwidth');
        $server->ddos_protection = request('ddosProtection') == 'on' ? true : false;
        $server->save();
        return redirect('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Server $server
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Server $server)
    {
       $server->delete();
       return redirect('/home');
    }
}
