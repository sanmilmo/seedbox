@extends('layouts.app')

@section('content')
<ul>
    @foreach($cloudServers as $cloudServer)
        <li>
            <a href="/servers/{{ $cloudServer->id }}">
            {{ $cloudServer->name }}
            </a>
        </li>
    @endforeach

</ul>
@endsection
