@extends('layouts.app')
@section('content')
    {!! Form::model($server, ['route' => ['servers.update', $server->id], 'method' => 'PUT']) !!}
        <div class="form-group">
            {!! Form::label('nameInput', 'Name:', []) !!}
            {!! Form::text("name", null, ["class" => "form-control", "id" => "nameInput"]) !!}
            {!! Form::label('descriptionInput', 'Description:', []) !!}
            {!! Form::text("description", null, ["class" => "form-control", "id" => "descriptionInput", "value" => ""]) !!}
            {!! Form::label('modelInput', 'Model:', []) !!}
            {!! Form::number("model", null, ["class" => "form-control", "id" => "modelInput", "value" => ""]) !!}
            {!! Form::label('opsInput', 'Operative System:', []) !!}
            {!! Form::select("ops", [
                    "Linux" => "Linux",
                    "Windows" => "Windows"
                ], null, ["class" => "form-control", "id" => "opsInput", "value" => ""]) !!}
            {!! Form::label('drivesInput', 'Drives:', []) !!}
            {!! Form::number("drives", null, ["class" => "form-control", "id" => "drivesInput", "value" => ""]) !!}
            {!! Form::label('driveTypeInput', 'Drive Type:', []) !!}
            {!! Form::select("driveType", [
                    "SSD" => "SSD",
                    "ATA" => "ATA",
                    "SATA" => "SATA",
                    "USB" => "USB",
                    "SCSI" => "SCSI"
                ], null, ["class" => "form-control", "id" => "driveTypeInput", "value" => ""]) !!}
            {!! Form::label('coresInput', 'CPU Cores:', []) !!}
            {!! Form::number("cores", null, ["class" => "form-control", "id" => "coresInput", "value" => ""]) !!}
            {!! Form::label('coresSpeedInput', 'CPU Cores Speed:', []) !!}
            {!! Form::number("cores_speed", null, ["class" => "form-control", "id" => "coresSpeedInput", "value" => ""]) !!}
            {!! Form::label('bandwidthInput', 'Bandwidth:', []) !!}
            {!! Form::number("bandwidth", null, ["class" => "form-control", "id" => "bandwidthInput", "value" => ""]) !!}
            {!! Form::label('ddosProtectionInput', 'DDOS Protection:', []) !!}
            {!! Form::checkbox("ddosProtection", null, ["class" => "form-control", "id" => "ddosProtectionInput", "value" => ""]) !!}
            {!! Form::submit('Submit', ["class" => "btn btn-primary ml-2"]) !!}
        </div>
    {!! Form::close() !!}
@endsection

