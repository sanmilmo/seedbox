@extends('layouts.app')

@section('content')
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <ul class="list-group mt-2">
        <li class="list-group-item"><h3>{{ $server->name }}</h3></li>
        <li class="list-group-item">
            <label class="font-weight-bold">Description:</label> {{ $server->description }} </li>
        <li class="list-group-item">
            <label class="font-weight-bold">Model:</label> {{ $server->model }} </li>
        <li class="list-group-item">
            <label class="font-weight-bold">Operative System:</label> {{ $server->ops }} </li>
        <li class="list-group-item">
            <label class="font-weight-bold">IP:</label> {{ $server->ip }} </li>
        <li class="list-group-item">
            <label class="font-weight-bold">Drives:</label> {{ $server->drives }} </li>
        <li class="list-group-item">
            <label class="font-weight-bold">Drive Type:</label> {{ $server->drive_type }} </li>
        <li class="list-group-item">
            <label class="font-weight-bold">Cores:</label> {{ $server->cores }} </li>
        <li class="list-group-item">
            <label class="font-weight-bold">Cores Speed:</label> {{ $server->cores_speed }} </li>
        <li class="list-group-item">
            <label class="font-weight-bold">Bandwidth:</label> {{ $server->bandwidth }} </li>
        <li class="list-group-item">
            @if ($server->ddos_protection == 1)
                <label class="font-weight-bold">DDOS Protection:</label> Active</li>
            @else
                <label class="font-weight-bold">DDOS Protection:</label> Inactive</li>
            @endif
    </ul>
    <div class="row pt=1">
        {!! Form::open([
                                                        'action' => ['ServerController@destroy', $server->id],
                                                        'method' => 'POST',
                                                        'class' => 'pull-right'
                                                    ]) !!}
        {{ Form::hidden('_method','DELETE') }}
        {{ Form::submit('X',['class' => 'btn btn-danger btn-sm mr-1']) }}
        {!! Form::close() !!}

        <a href="/servers/{{$server->id}}/edit">
            <button type="button" class="btn btn-secondary btn-sm p-1">edit</button>
        </a>
    </div>
@endsection

