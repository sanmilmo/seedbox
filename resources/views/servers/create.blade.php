@extends('layouts.app')
@section('content')
    {!! Form::open(['action' => 'ServerController@store', 'method' => 'post']) !!}
        <div class="form-group">
            {!! Form::label('nameInput', 'Name:', []) !!}
            {!! Form::text("name", "", ["class" => "form-control", "id" => "nameInput", "value" => "asasd"]) !!}
            {!! Form::label('descriptionInput', 'description:', []) !!}
            {!! Form::text("description", "", ["class" => "form-control", "id" => "descriptionInput", "value" => ""]) !!}
            {!! Form::label('modelInput', 'model:', []) !!}
            {!! Form::number("model", "", ["class" => "form-control", "id" => "modelInput", "value" => ""]) !!}
            {!! Form::label('opsInput', 'ops:', []) !!}
            {!! Form::select("ops", [
                    "Linux" => "Linux",
                    "Windows" => "Windows"
                ], null, ["class" => "form-control", "id" => "opsInput", "value" => ""]) !!}
            {!! Form::label('drivesInput', 'drives:', []) !!}
            {!! Form::number("drives", "", ["class" => "form-control", "id" => "drivesInput", "value" => ""]) !!}
            {!! Form::label('driveTypeInput', 'driveType:', []) !!}
            {!! Form::select("driveType", [
                    "SSD" => "SSD",
                    "ATA" => "ATA",
                    "SATA" => "SATA",
                    "USB" => "USB",
                    "SCSI" => "SCSI"
                ], null, ["class" => "form-control", "id" => "driveTypeInput", "value" => ""]) !!}
            {!! Form::label('coresInput', 'Cores:', []) !!}
            {!! Form::number("cores", "", ["class" => "form-control", "id" => "coresInput", "value" => ""]) !!}
            {!! Form::label('coresSpeedInput', 'coresSpeed:', []) !!}
            {!! Form::number("coresSpeed", "", ["class" => "form-control", "id" => "coresSpeedInput", "value" => ""]) !!}
            {!! Form::label('bandwidthInput', 'bandwidth:', []) !!}
            {!! Form::number("bandwidth", "", ["class" => "form-control", "id" => "bandwidthInput", "value" => ""]) !!}
            {!! Form::label('ddosProtectionInput', 'ddosProtection:', []) !!}
            {!! Form::checkbox("ddosProtection", "", ["class" => "form-control", "id" => "ddosProtectionInput", "value" => ""]) !!}
            {!! Form::submit('Submit', ["class" => "btn btn-primary ml-2"]) !!}
        </div>
    {!! Form::close() !!}
@endsection
