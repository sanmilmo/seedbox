@extends('layouts.app')
@section('content')
    <div class="container mt-md-3 mt-sm-0">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header grey-background"><h4>Server List</h4></div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="list-group">
                            <a href="/servers/create" class="ml-2 mb-2">Add New Server</a>
                            @php
                                $indicators = ["PING", "SSH", "HTTP", "HTTPS", "DNS", "SMTP" ];
                            @endphp
                            @foreach($cloudServers as $key => $cloudServer)
                                <div class="card mb-2">
                                    <div class="card-body">
                                        <div class="row">

                                            <a href="/servers/{{ $cloudServer->id }}"
                                               class="list-group-item list-group-item-action light-grey-background mb-1">
                                                {{ $cloudServer->name }}
                                            </a>

                                            {!! Form::open([
                                                'action' => ['ServerController@destroy', $cloudServer->id],
                                                'method' => 'POST',
                                                'class' => 'pull-right'
                                            ]) !!}
                                                {{ Form::hidden('_method','DELETE') }}
                                                {{ Form::submit('X',['class' => 'btn btn-danger btn-sm mr-1']) }}
                                            {!! Form::close() !!}

                                            <a href="/servers/{{$cloudServer->id}}/edit">
                                                <button type="button" class="btn btn-secondary btn-sm p-1">edit</button>
                                            </a>
                                            @php
                                                foreach($indicators as $indicator) {
                                                    $randBoolean = (bool)rand(0, 1);
                                                    //echo '<div class="">';
                                                    if ($randBoolean){
                                                        echo '<div class="col-sm"><i class="fas fa-check mr-1" style="font-size: 12px;"></i>' . $indicator .'</div>';
                                                    } else {
                                                        echo '<div class="col-sm"><i class="fas fa-times mr-1" style="font-size: 12px;"></i>' . $indicator .'</div>';
                                                    }
                                                    //echo '</div>';
                                                }
                                            @endphp
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
