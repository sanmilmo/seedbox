## Seedbox Test

### Instructions

- Rename .env-sample to .env
- php composer.phar install
- php artisan migrate
- npm install
- npm run dev
- php artisan serve