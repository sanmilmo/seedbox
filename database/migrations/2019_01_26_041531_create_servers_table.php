<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name');
            $table->string('description');
            $table->integer('model');
            $table->enum('ops', [
                "Windows",
                "Linux",
            ]);
            $table->string('ip');
            $table->integer('drives');
            $table->enum('drive_type', [
                "SSD",
                "ATA",
                "SATA",
                "USB",
                "SCSI"
            ]);
            $table->tinyInteger('cores');
            $table->float('cores_speed');
            $table->integer('bandwidth');
            $table->boolean('ddos_protection')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servers');
    }
}
